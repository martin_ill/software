package adderal.votoelectronico.partidoPolitico;

import java.util.List;
import java.util.ArrayList;
import adderal.votoelectronico.persona.Politico;

public class PartidoPolitico {
	private List<Politico> politicos;
	private String nombre;
	
	/*
	// Se crea el partido politico.
	*/
	public PartidoPolitico(String nombre, List<Politico> politicos) {
		this.nombre = nombre;
		this.politicos = new ArrayList<>(politicos);
	}

	public List<Politico> getPoliticos() {
		return politicos;
	}
	public void setPoliticos(List<Politico> politicos) {
		this.politicos = politicos;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
