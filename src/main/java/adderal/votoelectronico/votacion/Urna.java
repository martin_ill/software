package adderal.votoelectronico.votacion;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import adderal.votoelectronico.persona.Persona;

public class Urna {
	private Integer id;
	private Map<Persona, java.lang.Boolean> padron;
	private List<Voto> votos;
	
	/*
	*	La urna recibe la lista "personas" desde la ciudad que la creó.
	*	Se inicializa el arreglo de votos vacío.
	* 	Se inicializa el padron (HashMap) valuando a las personas en false (no voto).
	*/
	public Urna(Integer id, List<Persona> personas, int sizePadron) {	
		this.id = id;
		this.votos = new ArrayList<>(sizePadron);		 
		this.padron = new HashMap<>(sizePadron);

		for(int i=0; i < sizePadron; i++) 
			this.padron.put(personas.get(i), false);
	}

	/*
	**	Metodo a ejecutar a la hora de votar
	**	Chequea que la persona a votar se encuentre en el padron
	**	Registra el hecho del voto en el padron
	**	Guarda el voto
	*/
	public void votar(Persona votante, Voto voto) {
		if(padron.containsKey(votante)) {
			this.padron.replace(votante, true);
			this.votos.add(voto);
		}
		/*				
		else {						// si la persona no está en el padron, arroja excepcion
			throw Exception;
		}
		*/
	}

	public Integer getId() {
		return id;
	}
	
	public Map<Persona, java.lang.Boolean> getPadron() {
		return padron;
	}

	public List<Voto> getVotos() {
		return votos;
	}
}
