package adderal.votoelectronico.votacion;

import java.util.List;
import java.util.ArrayList;
import adderal.votoelectronico.persona.Politico;

public class Voto {
	private List<Politico> politicos;		// politicos a votar
	
	/*
	**	Una persona llama al constructor de Voto al ejecutar el metodo 'votar'
	**	El constructor recibe la lista de politicos y crea el voto
	*/
	public Voto(List<Politicos> politicos) {
		this.politicos = politicos;
	}

	public List<Politico> getPoliticos() {
		return politicos;
	}
}
