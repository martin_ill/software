package adderal.votoelectronico.persona;

import adderal.votoelectronico.partidoPolitico.PartidoPolitico;

public class Politico extends Persona {
	private PartidoPolitico partido;
	private String cargo;
	
	public Politico(String nombre, String cargo) {
		super(nombre);
		this.cargo = cargo;
	}

	public PartidoPolitico getPartido() {
		return partido;
	}
	public void setPartido(PartidoPolitico partido) {
		this.partido = partido;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
}
