package adderal.votoelectronico.persona;

import adderal.votoelectronico.estado.Estado;
import adderal.votoelectronico.votacion.Urna;

public class Persona {
	private String nombre;
	private Estado ciudad;
	private Urna urna;
	
	public Persona(String nombre, Estado ciudad) {
		this.nombre = nombre;
		this.ciudad = ciudad;
	}
	
	public String getNombre() {
		return nombre;
	}

	public Estado getCiudad() {
		return ciudad;
	}

	public Urna getUrna() {
		return urna;
	}

	public void setUrna(Urna urna) {
		this.urna = urna;
	}
}
