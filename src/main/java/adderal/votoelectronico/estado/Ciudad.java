package adderal.votoelectronico.estado;

import java.util.List;
import java.util.ArrayList;
import adderal.votoelectronico.votacion.Urna;

public class Ciudad extends Estado {
	private List<Urna> urnas;
	private final int PERSONAS_POR_URNA = 1000;

	/*
	**	Se crea la ciudad, creándose ademas las personas que perteneceran a la poblacion.
	**	FALTA MANEJO DE BASE DE DATOS PARA CREAR PERSONAS CON DISTINTOS NOMBRES. *
	*/
	public Ciudad(String nombre, Integer poblacion) {
		super(nombre, poblacion);
		personas = new ArrayList<>(poblacion);

		for(int i=0; i < poblacion; i++)
			personas.add(new Persona("Persona " + i, this));		// *
		setUrnas();
	} 

	public List<Urna> getUrnas() {
		return urnas;
	}

	/*
	**	Devuelve la cantidad de urnas que se deben crear, teniendo en cuenta
	**	que todas las urnas tendran el mismo tamaño y que la division 
	**	poblacion/PERSONAS_POR_URNA puede no ser entera.
	*/
	public int setCantidadUrnas() {

        Double cantidadUrnas = (double) this.poblacion / (double) PERSONAS_POR_URNA;
        if(cantidadUrnas > cantidadUrnas.intValue())
            return (cantidadUrnas.intValue() + 1);
        return cantidadUrnas.intValue();
    }

	/*
	**	Crea las urnas necesarias asociadas a la ciudad propiamente dicha.
	*/
    public void setUrnas() {

        int cantidadUrnas = setCantidadUrnas();
        this.urnas = new ArrayList<Urna>(cantidadUrnas);
        int j = 0;

        for(int i = 0; i < cantidadUrnas; i++) {
            ArrayList<Persona> personasEnUrna = new ArrayList<Persona>();                
            while(j < (PERSONAS_POR_URNA * (i + 1)) && j < this.poblacion) {
                personasEnUrna.add(this.personas.get(j));
                j++;
            }
            this.urnas.add(new Urna(i, personasEnUrna, personasEnUrna.size()));
        }
	}
	
	public void printUrnas() {
		for(Urna urna : urnas)
			System.out.println(getNombre() + " -> " + urna.getId());
	}

	public int getCantidadUrnas() {
		return urnas.size();
	}
}
