package adderal.votoelectronico.estado;

import java.util.List;
import java.util.ArrayList;

public class Pais extends Estado {
	private List<Provincia> provincias;
	private final int CANT_PROVINCIAS = 24;
	/*
	**	Clase pais.
	**	Simulacion argentina.
	**	La poblacion por provincia se reparte equitativamente: poblacion/CANT_CIUDADES 
	*/
	public Pais(String nombre, Integer poblacion) {
		super(nombre, poblacion);

		Double prov_pob = (double) poblacion / (double) CANT_PROVINCIAS;	
		int prov_pob_int = prov_pob.intValue();	// poblacion en entero (sin decimales)

		this.provincias = new ArrayList<>(CANT_PROVINCIAS);
		this.provincias.add(new Provincia("Buenos Aires", prov_pob_int));
		this.provincias.add(new Provincia("Catamarca", prov_pob_int));
		this.provincias.add(new Provincia("Chaco", prov_pob_int));
		this.provincias.add(new Provincia("Chubut", prov_pob_int));
		this.provincias.add(new Provincia("Cordoba", prov_pob_int));
		this.provincias.add(new Provincia("Corrientes", prov_pob_int));
		this.provincias.add(new Provincia("Entre Rios", prov_pob_int));
		this.provincias.add(new Provincia("Formosa", prov_pob_int));
		this.provincias.add(new Provincia("Jujuy", prov_pob_int));
		this.provincias.add(new Provincia("La Pampa", prov_pob_int));
		this.provincias.add(new Provincia("La Rioja", prov_pob_int));
		this.provincias.add(new Provincia("Mendoza", prov_pob_int));
		this.provincias.add(new Provincia("Misiones", prov_pob_int));
		this.provincias.add(new Provincia("Neuquen", prov_pob_int));
		this.provincias.add(new Provincia("Rio Negro", prov_pob_int));
		this.provincias.add(new Provincia("Salta", prov_pob_int));
		this.provincias.add(new Provincia("San Juan", prov_pob_int));
		this.provincias.add(new Provincia("San Luis", prov_pob_int));
		this.provincias.add(new Provincia("Santa Cruz", prov_pob_int));
		this.provincias.add(new Provincia("Santa Fe", prov_pob_int));
		this.provincias.add(new Provincia("Santiago del Estero", prov_pob_int));
		this.provincias.add(new Provincia("Tierra del Fuego", prov_pob_int));
		this.provincias.add(new Provincia("Tucuman", prov_pob_int));
		this.provincias.add(new Provincia("Falkland Islands", prov_pob_int));
	}
	
	public List<Provincia> getProvincias() {
		return provincias;
	}

	public void printProvincias() {
		System.out.println("\n" + getNombre());
		for(Provincia provincia : provincias)
			provincia.printCiudades();
	}
}
