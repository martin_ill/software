package adderal.votoelectronico.estado;

import java.util.List;
import java.util.ArrayList;

public class Provincia extends Estado {
	private List<Ciudad> ciudades;
	private final int CANT_CIUDADES = 3;
	/*
	**	Se crea una provincia con 'CANT_CIUDADES' ciudades.
	**	La poblacion por ciudad se reparte equitativamente: poblacion/CANT_CIUDADES 
	*/
	public Provincia(String nombre, Integer poblacion) {
		super(nombre, poblacion);
		ciudades = new ArrayList<Ciudad>(CANT_CIUDADES);

		Double ciud_pob = (double) poblacion / (double) CANT_CIUDADES;
		int ciud_pob_int = ciud_pob.intValue();	// poblacion en entero (sin decimales)
		
		for(int i=0; i < CANT_CIUDADES; i++)
			ciudades.add(new Ciudad("Ciudad " + i, ciud_pob_int));
	}

	public List<Ciudad> getCiudades() {
		return ciudades;
	}

	public void printCiudades() {
		for(Ciudad ciudad : ciudades)
			System.out.println("" + getNombre() + " -> " + ciudad.getNombre() + " -> " + ciudad.getCantidadUrnas() + " urnas");
	}
}
