package adderal.votoelectronico.estado;

import java.util.List;
import java.util.Map;
import adderal.votoelectronico.partidoPolitico.PartidoPolitico;
import adderal.votoelectronico.persona.Persona;
import adderal.votoelectronico.persona.Politico;

public class Estado {
	private String nombre;
	protected Integer poblacion;
	private List<PartidoPolitico> partidosPoliticos;
	protected List<Persona> personas;
	private Map<Politico,Integer> resultados;
	/*
	**	Se crea un estado.
	*/
	public Estado(String nombre, Integer poblacion) {
		this.nombre = nombre;
		this.poblacion = poblacion;
	}
	public String getNombre() {
		return nombre;
	}
	public Integer getPoblacion() {
		return poblacion;
	}
	public List<PartidoPolitico> getPartidosPoliticos() {
		return partidosPoliticos;
	}
	public List<Persona> getPersonas() {
		return personas;
	}
	public Map<Politico, Integer> getResultados() {
		return resultados;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPoblacion(Integer poblacion) {
		this.poblacion = poblacion;
	}
	public void setPartidosPoliticos(List<PartidoPolitico> partidosPoliticos) {
		this.partidosPoliticos = partidosPoliticos;
	}
	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	public void setResultados(Map<Politico, Integer> resultados) {
		this.resultados = resultados;
	}
}
